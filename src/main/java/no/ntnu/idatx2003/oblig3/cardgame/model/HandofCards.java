package no.ntnu.idatx2003.oblig3.cardgame.model;

import java.util.ArrayList;
import java.util.List;

public class HandofCards {
  private List<PlayingCard> cards;

  public HandofCards(List<PlayingCard> cards) {
    this.cards = cards;
  }

  public List<PlayingCard> getCards() {
    return cards;
  }

  public boolean flush() {
    char suit = cards.getFirst().getSuit();
    for (PlayingCard card : cards) {
      if (card.getSuit() != suit) {
        return false;
      }
    }
    return true;
  }

  public int sum() {
    int sum = 0;
    for (PlayingCard card : cards) {
      sum += card.getFace();
    }
    return sum;
  }

  public boolean hasQueenOfSpades() {
    for (PlayingCard card : cards) {
      if (card.getSuit() == 'S' && card.getFace() == 12) {
        return true;
      }
    }
    return false;
  }

  public List<PlayingCard> getHearts() {
    List<PlayingCard> hearts = new ArrayList<>();
    for (PlayingCard card : cards) {
      if (card.getSuit() == 'H') {
        hearts.add(card);
      }
    }
    return hearts;
  }
}
