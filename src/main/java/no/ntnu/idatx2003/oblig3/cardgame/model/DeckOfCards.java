package no.ntnu.idatx2003.oblig3.cardgame.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckOfCards {
  private final char[] suits = { 'S', 'H', 'D', 'C' };
  private List<PlayingCard> cards;

  public DeckOfCards() {
    cards = new ArrayList<PlayingCard>();
    for (char suit : suits) {
      for (int i = 1; i <= 13; i++) { // Start from 1, not 0
        cards.add(new PlayingCard(suit, i));
      }
    }
  }

  public List<PlayingCard> getCards() {
    return cards;
  }

  public HandofCards dealHand(int n) {
    if (n < 1 || n > 52) {
      throw new IllegalArgumentException("Parameter n must be between 1 and 52");
    }

    Random rand = new Random();
    List<PlayingCard> hand = new ArrayList<>();

    for (int i = 0; i < n; i++) {
      int randomIndex = rand.nextInt(cards.size());
      PlayingCard randomCard = cards.remove(randomIndex);
      hand.add(randomCard);
    }

    return new HandofCards(hand);
  }
}