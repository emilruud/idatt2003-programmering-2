package no.ntnu.idatx2003.oblig3.cardgame.view;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.idatx2003.oblig3.cardgame.model.DeckOfCards;
import no.ntnu.idatx2003.oblig3.cardgame.model.HandofCards;
import no.ntnu.idatx2003.oblig3.cardgame.model.PlayingCard;

public class CardGameGUI extends Application {

    private HBox cardsBox;

    private HandofCards currentHand; // Make currentHand a class member variable

   private Label sumNumberLabel;

    private Label flushLabel;

    private Label queenOfSpadesLabel;

    private Label heartsLabel;

    @Override
    public void start(Stage primaryStage) {
      BorderPane root = new BorderPane(); // Initialize root as a BorderPane
      cardsBox = new HBox(20); // Initialize cardsBox as an HBox with spacing of 20
        cardsBox.setAlignment(Pos.CENTER); // Set the alignment of the HBox to CENTER
        cardsBox.setPadding(new Insets(10, 10, 20, 10));
        //cardsBox.setStyle("-fx-border-color: red; -fx-border-width: 2;"); // Set border for cardsBox


        root.setTop(cardsBox); // Add cardsBox to the left region of the BorderPane

        root.setPadding(new Insets(50, 50, 20, 50)); // Set padding of the BorderPane

        Scene scene = new Scene(root, 800, 500);
        primaryStage.setTitle("Card Game");

      Text sumTextLabel = new Text("Sum of the faces: ");
        sumNumberLabel = new Label("0  ");
        HBox sumBox = new HBox(sumTextLabel, sumNumberLabel);// Create a HBox
        VBox leftBox = new VBox(sumBox);

        Text queenOfSpadesText = new Text("Has queen of Spades: ");
        queenOfSpadesLabel = new Label("   ");
        HBox queenOfSpadesBox = new HBox(queenOfSpadesText, queenOfSpadesLabel); // Create a HBox

        leftBox.getChildren().add(queenOfSpadesBox);

        leftBox.setAlignment(Pos.TOP_LEFT); // Set the alignment of the VBox to CENTER
        leftBox.setPadding(new Insets(10, 10, 10, 10));
        //leftBox.setStyle("-fx-border-color: blue; -fx-border-width: 2;"); // Set border for leftBox
        leftBox.setSpacing(20);

        Text flushText = new Text("Flush: ");
        flushLabel = new Label("   ");
        HBox flushBox = new HBox(flushText, flushLabel); // Create a HBox
        flushBox.setPadding(new Insets(10, 10, 10, 10));
        //flushBox.setStyle("-fx-border-color: green; -fx-border-width: 2;");
        VBox rightBox = new VBox(flushBox);

        Text heartsText = new Text("Cards of Hearts: ");
        heartsLabel = new Label(" ");
        HBox heartsBox = new HBox(heartsText, heartsLabel); // Create a HBox
        heartsBox.setPadding(new Insets(10, 10, 10, 10));
        //heartsBox.setStyle("-fx-border-color: green; -fx-border-width: 2;");
        rightBox.getChildren().add(heartsBox);


        HBox analytics = new HBox(leftBox);
        analytics.getChildren().add(rightBox);
        analytics.setSpacing(20);
        analytics.setPadding(new Insets(50, 10, 100, 10));
        analytics.setAlignment(Pos.BOTTOM_LEFT);
        //analytics.setStyle("-fx-border-color: red; -fx-border-width: 2;");


        root.setBottom(analytics);

        Button dealButton = new Button("Deal a new hand");
        dealButton.setOnAction(event -> {
            sumNumberLabel.setText("0");
            flushLabel.setText(" ");
            queenOfSpadesLabel.setText(" ");
            heartsLabel.setText(" ");
            DeckOfCards deck = new DeckOfCards();
            currentHand = deck.dealHand(5);
            displayHand(currentHand);
        });

        Button CheckButton = new Button("Check hand");
        CheckButton.setOnAction(event -> {
            int sumOfHand = currentHand.sum();
            sumNumberLabel.setText(Integer.toString(sumOfHand));
            displayFlush(currentHand.flush());
            displayQueenOfSpades(currentHand.hasQueenOfSpades());
            displayHearts(currentHand);
        });

        VBox buttonBox = new VBox(20); // Create a VBox
        buttonBox.setAlignment(Pos.TOP_RIGHT); // Set the alignment of the VBox to CENTER
        buttonBox.getChildren().add(dealButton); // Add the button to the VBox
        buttonBox.getChildren().add(CheckButton); // Add the button to the VBox
        //buttonBox.setStyle("-fx-border-color: blue; -fx-border-width: 2;"); // Set border for rightBox


        root.setRight(buttonBox); // Add the VBox to the right region of the BorderPane

        DeckOfCards deck = new DeckOfCards();
        currentHand = deck.dealHand(5);
        displayHand(currentHand);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void displayHand(HandofCards hand) {
        cardsBox.getChildren().clear();

        for (PlayingCard card : hand.getCards()) {
            Rectangle cardRectangle = new Rectangle(100, 150);
            cardRectangle.setArcWidth(10);
            cardRectangle.setArcHeight(10);
            cardRectangle.setFill(Color.WHITE);
            cardRectangle.setStroke(Color.BLACK);

            Text cardText = new Text(card.getAsString());

            StackPane cardPane = new StackPane();
            cardPane.getChildren().addAll(cardRectangle, cardText);

            cardsBox.getChildren().add(cardPane);
        }
    }

    private void displayFlush(boolean isFlush) {
        if (isFlush) {
            flushLabel.setText("Yes");
        } else {
            flushLabel.setText("No");
        }
    }

    private void displayQueenOfSpades(boolean hasQueenOfSpades) {
        if (hasQueenOfSpades) {
            queenOfSpadesLabel.setText("Yes");
        } else {
            queenOfSpadesLabel.setText("No");
        }
    }

    private void displayHearts(HandofCards hand) {
        String hearts = "";
        for (PlayingCard card : hand.getHearts()) {
            hearts += card.getAsString() + " ";
        }
        heartsLabel.setText(hearts);
    }

    public static void main(String[] args) {
        launch(args);
    }
}