package no.ntnu.idatx2003.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.assertEquals;

import no.ntnu.idatx2003.oblig3.cardgame.model.PlayingCard;
import org.junit.jupiter.api.Test;

public class PlayingCardTest {
    @Test
    public void testGetAsString() {
        PlayingCard card = new PlayingCard('H', 4);
        assertEquals("H4", card.getAsString());
    }

    @Test
    public void testGetSuit() {
        PlayingCard card = new PlayingCard('H', 4);
        assertEquals('H', card.getSuit());
    }
}